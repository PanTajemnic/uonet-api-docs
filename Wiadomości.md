# Wiadomości

## Uzyskiwanie wiadomości odebranych

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/WiadomosciOdebrane
```

```json
{
    "DataPoczatkowa": 1517439600,
    "DataKoncowa": 1535666400,
    "LoginId": 5481,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1528702591,
    "TimeKey": 1528702590,
    "RequestId": "34555470-07b5-4d44-bde8-5b5a4ff7a424",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1528702591,
    "TimeValue": "2018.06.11 9:36:30",
    "RequestId": "34555470-07b5-4d44-bde8-5b5a4ff7a424",
    "DayOfWeek": 1,
    "AppVersion": "18.01.0002.27350",
    "Data": [
        {
            "WiadomoscId": 27214,
            "Nadawca": "Nazwisko Imię",
            "NadawcaId": 3617,
            "Adresaci": null,
            "Tytul": "Temat wiadomości",
            "Tresc": "Tak wygląda zawartość wiadomości.\nZazwyczaj ma wiele linijek.\n\nZ poważaniem,\nNazwisko Imię",
            "DataWyslania": "01.03.2018",
            "DataWyslaniaUnixEpoch": 1519911076,
            "GodzinaWyslania": "14:31",
            "DataPrzeczytania": null,
            "DataPrzeczytaniaUnixEpoch": null,
            "GodzinaPrzeczytania": null,
            "StatusWiadomosci": "Widoczna",
            "FolderWiadomosci": "Odebrane",
            "Nieprzeczytane": null,
            "Przeczytane": null
        },
        {
            "WiadomoscId": 28973,
            "Nadawca": "Kowalski Jan",
            "NadawcaId": 2137,
            "Adresaci": null,
            "Tytul": "Tytuł",
            "Tresc": "Dużo różnych treści.\nBardzo dużo,\nbardzo długie to potrafi być",
            "DataWyslania": "05.04.2018",
            "DataWyslaniaUnixEpoch": 1522921875,
            "GodzinaWyslania": "11:51",
            "DataPrzeczytania": null,
            "DataPrzeczytaniaUnixEpoch": null,
            "GodzinaPrzeczytania": null,
            "StatusWiadomosci": "Widoczna",
            "FolderWiadomosci": "Odebrane",
            "Nieprzeczytane": null,
            "Przeczytane": null
        }
    ]
}
```

## Uzyskiwanie wiadomości wysłanych

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/WiadomosciWyslane
```

```json
{
    "DataPoczatkowa": 1517439600,
    "DataKoncowa": 1535666400,
    "LoginId": 5481,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1528702600,
    "TimeKey": 1528702599,
    "RequestId": "f51ef094-991f-4430-9518-b1eb3b944868",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1528703157,
    "TimeValue": "2018.06.11 9:45:56",
    "RequestId": "2426e0e8-0c4c-4fff-90a2-c07436b9e1d0",
    "DayOfWeek": 1,
    "AppVersion": "18.01.0002.27350",
    "Data": [
        {
            "WiadomoscId": 32798,
            "Nadawca": null,
            "NadawcaId": 0,
            "Adresaci": [
                {
                    "LoginId": 3604,
                    "Nazwa": "Nazwisko Imię - pracownik [OpolePSP2137]"
                }
            ],
            "Tytul": "Usprawiedliwienie nieobecności",
            "Tresc": "Proszę o usprawiedliwienie nieobecności mojego syna Micksona w dniu 17 lipca 2018 r. z powodu walenia wiadra :DDD :-DD",
            "DataWyslania": "11.06.2018",
            "DataWyslaniaUnixEpoch": 1528702715,
            "GodzinaWyslania": "09:38",
            "DataPrzeczytania": null,
            "DataPrzeczytaniaUnixEpoch": null,
            "GodzinaPrzeczytania": null,
            "StatusWiadomosci": "Widoczna",
            "FolderWiadomosci": "Wyslane",
            "Nieprzeczytane": "1",
            "Przeczytane": "0"
        }
    ]
}
```

## Uzyskiwanie wiadomości usuniętych

Wiadomości nie są naprawdę usuwane, a jedynie przenoszone do "wiadomości usuniętych", czyli krótko mówiąc - "kosza".

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/WiadomosciUsuniete
```

```json
{
    "DataPoczatkowa": 1517439600,
    "DataKoncowa": 1535666400,
    "LoginId": 5481,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1528703212,
    "TimeKey": 1528703211,
    "RequestId": "f1e8dc80-bbd0-4248-8a97-d72824dfb608",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1528703213,
    "TimeValue": "2018.06.11 9:46:52",
    "RequestId": "f1e8dc80-bbd0-4248-8a97-d72824dfb608",
    "DayOfWeek": 1,
    "AppVersion": "18.01.0002.27350",
    "Data": [
        {
            "WiadomoscId": 89626,
            "Nadawca": "Nazwisko Imię",
            "NadawcaId": 3604,
            "Adresaci": [],
            "Tytul": "FREE SPOILER MESSAGE :-DD :DDDD",
            "Tresc": "NOD FOR RAEDING\n:-DDD :DDDD",
            "DataWyslania": "06.02.2018",
            "DataWyslaniaUnixEpoch": 1517938585,
            "GodzinaWyslania": "18:36",
            "DataPrzeczytania": "27.04.2018",
            "DataPrzeczytaniaUnixEpoch": 1524844708,
            "GodzinaPrzeczytania": "17:58",
            "StatusWiadomosci": "Usunieta",
            "FolderWiadomosci": "Odebrane",
            "Nieprzeczytane": null,
            "Przeczytane": null
        }
    ]
}
```

## Zmiana statusu wiadomości

Czyli przekazywanie informacji o odczytaniu wiadomości i przenoszenie ich do kosza.

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/ZmienStatusWiadomosci
```

### Odczytanie wiadomości

```json
{
    "WiadomoscId": 27214,
    "FolderWiadomosci": "Odebrane",
    "Status": "Widoczna",
    "LoginId": 5481,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1528703162,
    "TimeKey": 1528703161,
    "RequestId": "5b5401ae-f603-4488-9a4d-1e7cb55c7e6b",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

`FolderWiadomosci` to folder, w którym znajduje się obecnie wiadomość.

`Status` oznacza co konkretnie stało się z wiadomością - `Widoczna` znaczy po prostu, że została odczytana.

### "Usuwanie" wiadomości

```json
{
    "WiadomoscId": 25704,
    "FolderWiadomosci": "Odebrane",
    "Status": "Usunieta",
    "LoginId": 5481,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1528703209,
    "TimeKey": 1528703208,
    "RequestId": "7283a87e-0802-448f-9db6-bb1d74aa173b",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

`Status: Usunieta` znaczy, że wiadomość została przeniesiona do folderu "Wiadomości usunięte"

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1528703162,
    "TimeValue": "2018.06.11 9:46:01",
    "RequestId": "5b5401ae-f603-4488-9a4d-1e7cb55c7e6b",
    "DayOfWeek": 1,
    "AppVersion": "18.01.0002.27350",
    "Data": "Zmiana statusu wiadomości."
}
```

## Wysyłanie wiadomości

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/DodajWiadomosc
```

```json
{
    "NadawcaWiadomosci": "Nazwisko Imię (twoje)",
    "Tytul": "Usprawiedliwienie nieobecności",
    "Tresc": "Proszę o usprawiedliwienie nieobecności mojego syna Micksona w dniu 17 lipca 2018 r. z powodu walenia wiadra :DDD :-DD",
    "Adresaci": [
        {
            "LoginId": 3604,
            "Nazwa": "Nazwisko Imię - pracownik [OpolePSP2137]"
        }
    ],
    "LoginId": 5481,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1528702715,
    "TimeKey": 1528702714,
    "RequestId": "0d42c2f4-e3dc-481b-bc78-c04e34322955",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź (proszę nie regulować monitorów, to jest prawdziwa odpowiedź):

```json
{
    "Status": "Ok",
    "TimeKey": 1528702715,
    "TimeValue": "2018.06.11 9:38:35",
    "RequestId": "0d42c2f4-e3dc-481b-bc78-c04e34322955",
    "DayOfWeek": 1,
    "AppVersion": "18.01.0002.27350",
    "Data": {
        "WiadomoscId": 32798,
        "Nadawca": null,
        "NadawcaId": 0,
        "Adresaci": null,
        "Tytul": null,
        "Tresc": null,
        "DataWyslania": "2018-06-11",
        "DataWyslaniaUnixEpoch": 0,
        "GodzinaWyslania": "09:38",
        "DataPrzeczytania": null,
        "DataPrzeczytaniaUnixEpoch": null,
        "GodzinaPrzeczytania": null,
        "StatusWiadomosci": "Widoczna",
        "FolderWiadomosci": 0,
        "Nieprzeczytane": null,
        "Przeczytane": null
    }
}
```
